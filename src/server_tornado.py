import tornado.web
from tornado.ioloop import IOLoop
from tornado.log import app_log, enable_pretty_logging
from tornado.concurrent import run_on_executor
from concurrent.futures import ThreadPoolExecutor

try:
    import simplejson as json
except ImportError:
    import json

import time
import logging
import prometheus_client
from datetime import datetime


class Config:
    API_PORT = 8888
    METRICS_PORT = 8889
    VERSION = "0.1"
    RUN_MULTI_PROCESS = False
    NUM_WORKER_THREADS = 4

class Metrics:
    COUNTER_REQ = prometheus_client.Counter('search_num_req', "Number of requests")
    COUNTER_EXC = prometheus_client.Counter('search_num_exception', "Number of exceptions")
    LATENCY_REQ = prometheus_client.Summary('search_req_latency', "Serverside request latency") 

class SearchBaseHandler(tornado.web.RequestHandler):
    executor = ThreadPoolExecutor(Config.NUM_WORKER_THREADS)
    
    def initialize(self):
        self.rank_response = {}
        self.jsonargs = {}
        
    def prepare(self):
        """ Called before GET/POST handler """
        Metrics.COUNTER_REQ.inc()
        self.start_time = datetime.now()
    
    def on_finish(self):
        req_time = datetime.now() - self.start_time
        Metrics.LATENCY_REQ.observe(req_time.total_seconds() * 1000)
        
        log_message = {
            'request': self.jsonargs,
            'response': self.rank_response
        }
        app_log.info(json.dumps(log_message))
        
    
    async def get(self):
        """ GET handler """
        with Metrics.COUNTER_EXC.count_exceptions():
            await self.process_request()
    
    
    def parse_input(self, reqbody):
        try:
            self.jsonargs = json.loads(reqbody)
        except json.JSONDecodeError as e:
            app_log.exception("Invalid request data %s", e)
            raise tornado.web.HTTPError(status_code=400, log_message="Invalid JSON data", reason="Invalid input")
    
    
    def send_json(self):
        rank_json = json.dumps(self.rank_response)
        self.set_header("Content-Type", "application/json")
        self.write(rank_json)
    
    async def process_request(self):
        self.parse_input(self.request.body)
        await self.rank_results()    
        self.send_json()

    async def rank_results(self):
        """Search ranking results, get input as dict"""
        def call_model():
            arg_dict = self.jsonargs        
            assert self.jsonargs
            time.sleep(0.1)
            self.rank_response = {
                "request_id": "r112313",
                "results": [
                    {
                        "item_id": "abcd213",
                        "score": 0.987
                    },
            ]}
            app_log.info("Model call completed")
            
            
        await IOLoop.current().run_in_executor(self.executor, call_model)

def make_app():
    server_info = prometheus_client.Info('server_info', "Server Information")
    server_info.info({
        "name": "Search ranking server",
        "version": Config.VERSION,
        "start_time": datetime.now().isoformat()
    })

    return tornado.web.Application([
        (r"/", SearchBaseHandler),
    ])
    
def startup_log(): 
    enable_pretty_logging()
    app_log.info("Starting server: %s", Config.VERSION)
    app_log.info("API Port: %d", Config.API_PORT)
    app_log.info("Metrics Port: %d", Config.METRICS_PORT)
    
def run_server():
    startup_log()
    
    prometheus_client.start_http_server(Config.METRICS_PORT)
    
    app = make_app()
    app_log.info("Created Tornado app")
    
    if Config.RUN_MULTI_PROCESS:
        app_log.info("starting multi process")
        server = tornado.httpserver.HTTPServer(app)
        server.bind(Config.API_PORT)
        server.start(0)  # forks one process per cpu
    else:
        app_log.info("starting single process")
        app.listen(Config.API_PORT)
    
    IOLoop.current().start()
    

if __name__ == "__main__":
    run_server()
